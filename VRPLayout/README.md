**Project: Solving Vehicle Routing Problem and its variants using quantum computing (Group 2 - QAOA) **

**Mentor:** Paweł Gora

**Description:** Vehicle Routing Problem (VRP) is a combinatorial optimization problem important for real-world logistics and difficult (NP-hard) from a computational perspective. The goal is to find optimal routes of a fleet of vehicles aiming to visit some number of locations. There are different variants of VRP, e.g., with limited capacities of vehicles, time windows for visiting specific locations, multiple depots etc. All of them are interesting areas for applications of quantum computers.

To develop and compare current quantum optimisation methods in the VRP, this research group is completing work in three primary areas. This includes:

- Quantum Optimisation:
    - Variational Quantum Eigensolver
    - Quantum Annealing
    - Quantum Approximate Optimisation Algorithm

- Graph network representation and topology reduction:
    - Graph Coarsening
    - 

- Classical Optimisation:
    - VRP as a Mixed Integer Linear Program

Please read the Maintainers Guide on how to contribute to this repository
